#!/bin/bash

for SS in 1 2 3 4 5 6 7 8 9 10 30
do
  for TTFW in 0.2 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0
  do
    #sed -i "s/SS=[0-9]\+\.[0-9]\+/SS=$SS/" common_astar_script.sh;
    #sed -i "s/TTFW=[0-9]\+\.[0-9]\+/TTFW=$TTFW/" common_astar_script.sh;
    qsub -v SS=$SS,TTFW=$TTFW common_astar_script.sh
  done
done
