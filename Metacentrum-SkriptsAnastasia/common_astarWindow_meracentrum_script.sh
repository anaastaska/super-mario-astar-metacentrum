#!/bin/bash

for SS in 1 2 3 4 5 6 7 8 9 10 30
do
  for RIGHT_BORDER_X in 65.00 75.00 95.00 120.00 135.00 150.00 165.00 200.00 215.00 235.00 255.00
  do
    #sed -i "s/SS=[0-9]\+\.[0-9]\+/SS=$SS/" common_astar_script.sh;
    #sed -i "s/RIGHT_BORDER_X=[0-9]\+\.[0-9]\+/RIGHT_BORDER_X=$RIGHT_BORDER_X/" common_astarWindow_script.sh;
    #qsub common_astarWindow_script.sh;
    
    qsub -v SS=$SS,RIGHT_BORDER_X=$RIGHT_BORDER_X common_astarWindow_script.sh
  done
done
